Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bliss
Upstream-Contact:
 Tommi Junttila
 Petteri Kaski
Source: http://www.tcs.hut.fi/Software/bliss/

Files: *
Copyright:
 2003-2020 Tommi Junttila
License: LGPL-3+

Files: debian/*
Copyright:
 2016-2020 Jerome Benoit <calculus@rezozer.net>
 2009-2013 David Bremner <bremner@unb.ca>
License: GPL-3+

Files: debian/adhoc/examples/myciel3.col
Copyright:
 1994-2020 Michael Trick <trick@cmu.edu>
License: public-domain
 This file is public domain and comes with NO WARRANTY of any kind.
Comment:
 This data file belongs to the Graph Coloring Instances collection,
 a collection provided at https://mat.tepper.cmu.edu/COLOR/instances.html
 by Michael Trick <trick@cmu.edu>. It was grabbed as-is by hand.

License: LGPL-3+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-3'.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.
